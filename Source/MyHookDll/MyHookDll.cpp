// MyHookDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "MyHoookDll.h"

HHOOK hMyHook=NULL;
HINSTANCE hInstance;

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (wParam == WM_LBUTTONDOWN)
	{
		return true;
	}
	return CallNextHookEx(hMyHook, nCode, wParam, lParam);
}

void initMouseHook(HWND hwndWindow)
{
	if (hMyHook != NULL) return;
	hMyHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)MouseProc, 0, 0);
}
void unMouseHook()
{
	if (hMyHook != NULL)
	{
		UnhookWindowsHookEx(hMyHook);
		hMyHook = NULL;
	}
}


